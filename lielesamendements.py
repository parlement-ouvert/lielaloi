#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Ajoute à chaque amendement le lien vers l'article auquel il fait référence."""


import argparse
import os
import re
import sys

from slugify import slugify

from elaguelaloi import extract_table_of_contents_local_url_paths


article_re = re.compile(r"(?ims)(^|[ '’])(?P<article>article\s+(\d+|1er|premier))")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'bill_dir',
        help='path of directory containing Markdown text files of a bill',
        )
    parser.add_argument(
        'amendments_dir',
        help='path of directory containing Markdown text files of amendments',
        )
    args = parser.parse_args()

    bill_dir = os.path.abspath(os.path.normpath(args.bill_dir))
    bill_dir = os.path.splitext(bill_dir)[0]
    assert os.path.isdir(bill_dir)
    bill_table_of_contents_file_path = bill_dir + '.md'
    assert os.path.exists(bill_table_of_contents_file_path)
    bill_url_path = '/' + os.path.basename(bill_dir)
    bill_leaf_url_paths = extract_table_of_contents_local_url_paths(bill_url_path, bill_table_of_contents_file_path)
    bill_leaf_url_path_by_slug = {
        url_path.rsplit('/', 1)[-1]: url_path
        for url_path in bill_leaf_url_paths
        }

    amendments_dir = os.path.abspath(os.path.normpath(args.amendments_dir))
    amendments_dir = os.path.splitext(amendments_dir)[0]
    assert os.path.isdir(amendments_dir)
    amendments_table_of_contents_file_path = amendments_dir + '.md'
    assert os.path.exists(amendments_table_of_contents_file_path)
    for parent_dir, dir_names, filenames in os.walk(amendments_dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            file_path = os.path.join(parent_dir, filename)
            dir_path = os.path.splitext(file_path)[0]
            if os.path.exists(dir_path):
                # Markdown file is not a leaf (not an amendment) => Skip it.
                continue
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                current_text = markdown_file.read()
            new_text = current_text
            article_match = article_re.search(new_text)
            if article_match is None:
                print('Article not found in {}'.format(file_path))
                continue
            article_number = article_match.group('article')
            article_slug = slugify(article_number).replace('-1er', '-1').replace('-premier', '-1')
            article_path = bill_leaf_url_path_by_slug.get(article_slug)
            if article_path is None:
                print('Article {} ({}) not found'.format(article_slug, article_number))
                continue
            new_text = '{}[{}]({}){}'.format(
                new_text[:article_match.start('article')],
                article_number,
                article_path,
                new_text[article_match.end('article'):],
                )
            if new_text != current_text:
                print('Updated link to article in {}'.format(file_path))
                with open(file_path, 'w', encoding='utf-8') as markdown_file:
                    markdown_file.write(new_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
