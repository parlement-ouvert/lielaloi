#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Parcourt l'arborescence d'un document et vérifie sa cohérence.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys


local_link_re = re.compile(r'''(?ims)\]\(\s*(?P<url_path>/.*?)(\s|\))''')


def extract_table_of_contents_local_url_paths(table_of_contents_url_path, table_of_contents_file_path):
    with open(table_of_contents_file_path, 'r', encoding='utf-8') as table_of_contents_file:
        table_of_contents = table_of_contents_file.read()
    local_url_paths = []
    table_of_contents_url_path_with_trailing_slash = (table_of_contents_url_path
        if table_of_contents_url_path.endswith('/')
        else table_of_contents_url_path + '/')
    for local_link_match in local_link_re.finditer(table_of_contents):
        url_path = local_link_match.group('url_path')
        if url_path.startswith(table_of_contents_url_path_with_trailing_slash):
            assert url_path not in local_url_paths, url_path
            local_url_paths.append(url_path)
    return local_url_paths


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    root_dir = os.path.abspath(os.path.normpath(args.dir))
    assert os.path.isdir(root_dir)
    table_of_contents_path = os.path.join(root_dir, 'home.md')
    assert os.path.exists(table_of_contents_path)

    found_url_paths = set()
    url_paths_by_table_of_contents_url_path = {}
    for parent_dir, dir_names, filenames in os.walk(root_dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            if filename == 'README.md':
                continue
            if parent_dir == root_dir and filename == 'home.md':
                continue

            file_path = os.path.join(parent_dir, filename)
            url_path = '/' + os.path.splitext('/'.join(
                name
                for name in file_path[len(root_dir):].split(os.sep)
                if name
                ))[0]
            found_url_paths.add(url_path)

            filename_core = os.path.splitext(filename)[0]
            if filename_core in dir_names:
                url_paths_by_table_of_contents_url_path[url_path] = extract_table_of_contents_local_url_paths(
                    url_path, file_path)

    for table_of_contents_url_path, url_paths in sorted(url_paths_by_table_of_contents_url_path.items()):
        table_of_contents_url_path_with_trailing_slash = (table_of_contents_url_path
            if table_of_contents_url_path.endswith('/')
            else table_of_contents_url_path + '/')
        for url_path in url_paths:
            if url_path not in found_url_paths:
                print("Table of contents at {} refers to {} that doesn't exist".format(
                    table_of_contents_url_path, url_path))
        for url_path in sorted(found_url_paths):
            if url_path.startswith(table_of_contents_url_path_with_trailing_slash) and url_path not in url_paths:
                print("Table of contents at {} misses a reference to {}".format(table_of_contents_url_path, url_path))

    return 0


if __name__ == '__main__':
    sys.exit(main())
