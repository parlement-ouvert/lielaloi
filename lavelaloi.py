#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Nettoie les textes, et notamment les liens erronés, des documents d'un dossier législatif.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys


absolute_urls_re = re.compile(r'''(?<=[\("])https://donnees-personnelles\.parlement-ouvert\.fr/''')
articles_url_path_re = re.compile(r"(?ims)\[(?P<fragment>[^[]*?)\]\(.*?/articles/.*?\)")
carriage_return_re = re.compile(r"\r")
jsessionid_re = re.compile(r'(?ims);jsessionid=.*?(?=[\?;/])')
line_feeds_re = re.compile(r'\n{3,}')
trailing_spaces_re = re.compile(r"(?ims)[ \t]+$")


def remove_absolute_urls(text):
    return absolute_urls_re.sub(r'/', text)


def remove_carriage_returns(text):
    return carriage_return_re.sub(r'\n', text)


def remove_jsessionid(text):
    return jsessionid_re.sub(r'', text)


def remove_line_feeds(text):
    return line_feeds_re.sub(r'\n\n', text)


def remove_links_with_articles_url_path(text):
    return articles_url_path_re.sub(r'\g<fragment>', text)


def remove_trailing_spaces(text):
    return trailing_spaces_re.sub(r'', text)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    for parent_dir, dir_names, filenames in os.walk(args.dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            if filename == 'README.md':
                continue
            file_path = os.path.join(parent_dir, filename)
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                text = markdown_file.read()
            clean_text = text
            clean_text = remove_carriage_returns(clean_text)
            clean_text = remove_trailing_spaces(clean_text)
            clean_text = remove_line_feeds(clean_text)
            clean_text = remove_absolute_urls(clean_text)
            clean_text = remove_jsessionid(clean_text)
            clean_text = remove_links_with_articles_url_path(clean_text)
            if clean_text != text:
                print('Cleaning {}'.format(file_path))
                with open(file_path, 'w', encoding='utf-8') as markdown_file:
                    markdown_file.write(clean_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
