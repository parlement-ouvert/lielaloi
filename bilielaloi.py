#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Ajoute à chaque article les liens vers les pages qui lui font référence.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys

from elaguelaloi import extract_table_of_contents_local_url_paths
from lavelaloi import remove_line_feeds, remove_trailing_spaces


local_link_re = re.compile(r'''(?ims)\]\(\s*(?P<url_path>/.*?)(\s|\))''')
paginator_re = re.compile('''(?ims)^\s*<div\s+class="forum-ignore".*?</div>''')
references_re = re.compile(r'''(?ims)^\s*?<!--\s*DEBUT\s+REFERENCES\s*-->.*?<!--\s*FIN\s+REFERENCES\s*-->\s*?$''')
subtitle_re = re.compile(r'''(?ims)^\s*<!--\s*SUBTITLE:\s*(?P<subtitle>.*?)\s*-->''')
title_re = re.compile(r'''(?ims)^\s*<!--\s*TITLE:\s*(?P<title>.*?)\s*-->''')


def remove_paginators(text):
    return paginator_re.sub(r'', text)


def remove_references(text):
    return references_re.sub(r'', text)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    node_by_url_path = {}
    root_dir = os.path.abspath(os.path.normpath(args.dir))
    for parent_dir, dir_names, filenames in os.walk(root_dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
            elif dir_name in ('legifrance', 'loi-2018', 'prive'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            if filename.startswith('index-'):
                continue
            if filename == 'README.md':
                continue
            if parent_dir == root_dir and filename in ('home.md', 'legifrance.md', 'loi-2018.md'):
                continue
            file_path = os.path.join(parent_dir, filename)
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                current_text = markdown_file.read()
            new_text = current_text
            new_text = remove_paginators(new_text)
            new_text = remove_references(new_text)

            url_path = '/' + os.path.splitext('/'.join(
                name
                for name in file_path[len(root_dir):].split(os.sep)
                if name
                ))[0]
            node = node_by_url_path.setdefault(url_path, {})
            title_match = title_re.search(new_text)
            assert title_match is not None, 'Missing title in {}'.format(file_path)
            node['title'] = title_match.group('title')
            subtitle_match = subtitle_re.search(new_text)
            if subtitle_match is not None:
                node['subtitle'] = subtitle_match.group('subtitle')

            for local_link_match in local_link_re.finditer(new_text):
                target_url_path = local_link_match.group('url_path')
                if target_url_path.startswith(url_path + '/'):
                    # Skip references from a node to a child.
                    continue
                node_by_url_path.setdefault(target_url_path, {}).setdefault('references', set()).add(url_path)

    # Build an ordered list of url_paths:
    ordered_url_paths = []
    for filename in os.listdir(root_dir):
        if filename.startswith('.'):
            continue
        if not filename.endswith('.md'):
            continue
        if filename == 'legifrance.md':
            continue
        dir = os.path.join(root_dir, os.path.splitext(filename)[0])
        if not os.path.isdir(dir):
            continue
        url_path = '/' + os.path.basename(dir)
        file_path = os.path.join(root_dir, filename)
        local_url_paths = extract_table_of_contents_local_url_paths(url_path, file_path)
        ordered_url_paths.extend(local_url_paths)

    for parent_dir, dir_names, filenames in os.walk(root_dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
            elif dir_name in ('legifrance', 'prive'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            if filename.startswith('index-'):
                continue
            if filename == 'README.md':
                continue
            if parent_dir == root_dir and filename in ('home.md', 'legifrance.md'):
                continue
            file_path = os.path.join(parent_dir, filename)
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                current_text = markdown_file.read()
            new_text = current_text
            new_text = remove_references(new_text)

            url_path = '/' + os.path.splitext('/'.join(
                name
                for name in file_path[len(root_dir):].split(os.sep)
                if name
                ))[0]

            references_url_path = (url_path.replace('/loi-2018', '/loi-78-17', 1)
                if url_path.startswith('/loi-2018')
                else url_path)
            node = node_by_url_path.get(references_url_path, {})
            references = node.get('references')

            if references:
                paginator_matches = list(paginator_re.finditer(new_text))
                if len(paginator_matches) != 2:
                    print('Wrong number of paginators in {}: {}'.format(file_path, len(paginator_matches)))
                    continue
                last_paginator_start = paginator_matches[-1].start()
                source_url_paths_by_document_url_path = {}
                for source_url_path in ordered_url_paths:
                    if source_url_path not in references:
                        continue
                    source_document_url_path = '/'.join(source_url_path.split('/')[:2])
                    source_url_paths_by_document_url_path.setdefault(source_document_url_path, []).append(
                        source_url_path)
                lines = []
                for source_document_url_path, source_url_paths in sorted(source_url_paths_by_document_url_path.items()):
                    link_source_document = False
                    links = []
                    for source_url_path in source_url_paths:
                        if source_url_path == source_document_url_path:
                            link_source_document = True
                            continue
                        source_node = node_by_url_path[source_url_path]
                        links.append(
                            '  * [{}]({})'.format(
                                ' — '.join(filter(None, [source_node['title'], source_node.get('subtitle')])),
                                source_url_path,
                                )
                            )
                    source_document_node = node_by_url_path[source_document_url_path]
                    source_document_text = ' — '.join(filter(None, [
                        source_document_node['title'],
                        source_document_node.get('subtitle'),
                        ]))
                    lines.append(
                        '* [{}]({})'.format(source_document_text, source_document_url_path)
                            if link_source_document
                            else '* {}'.format(source_document_text)
                        )
                    lines.extend(links)
                references_block = '''\
<!-- DEBUT REFERENCES -->

----

# Références

_Documents faisant référence à cette page :_

{}

<!-- FIN REFERENCES -->
'''.format('\n'.join(lines))
                new_text = '{}\n{}\n{}'.format(
                    new_text[:last_paginator_start],
                    references_block,
                    new_text[last_paginator_start:],
                    )
            new_text = remove_trailing_spaces(new_text)
            new_text = remove_line_feeds(new_text)
            if new_text != current_text:
                print('Updated references to {}'.format(file_path))
                with open(file_path, 'w', encoding='utf-8') as markdown_file:
                    markdown_file.write(new_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
