#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère les liens externes des documents d'un dossier législatif.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys


external_urls_re = re.compile(r'''(?<=[\("])(?P<url>https?://.+?)(?=\s*[\)"])''')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    external_urls = set()
    for parent_dir, dir_names, filenames in os.walk(args.dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            if filename == 'README.md':
                continue
            file_path = os.path.join(parent_dir, filename)
            with open(file_path, 'r', encoding='utf-8') as markdown_file:
                text = markdown_file.read()
            external_urls.update(
                external_url_match.group('url')
                for external_url_match in external_urls_re.finditer(text)
                )
    for external_url in sorted(external_urls):
        print(external_url)

    return 0


if __name__ == '__main__':
    sys.exit(main())
