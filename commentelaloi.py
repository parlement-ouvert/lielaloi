#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Met à jour les topics du forum Discourse servant aux commentaires sur le dossier législatif."""


import argparse
import configparser
import itertools
import json
import logging
import os
import re
import shutil
import sys
import time
import urllib.error
import urllib.parse
import urllib.request

from lxml import etree


app_name = os.path.splitext(os.path.basename(__file__))[0]
config = None
forum_url = None
html_parser = etree.HTMLParser()
log = logging.getLogger(app_name)


def delete(path):
    query = urllib.parse.urlencode(dict(
        api_key=config['forum_api_key'],
        api_username=config['forum_username'],
        ))
    request = urllib.request.Request(
        method = 'DELETE',
        url = urllib.parse.urljoin(forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            # return json.loads(response.read().decode('utf-8'))
            return None
    else:
        raise Exception("Too many errors.")


def get(path):
    query = urllib.parse.urlencode(dict(
        api_key=config['forum_api_key'],
        api_username=config['forum_username'],
        ))
    request = urllib.request.Request(
        url = urllib.parse.urljoin(forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('config_path', help='path of configuration file')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='increase output verbosity')
    args = parser.parse_args()

    parsed_config = configparser.ConfigParser()
    parsed_config.read(args.config_path)
    global config
    forum_articles_prefix = parsed_config['forum_articles_prefix']
    config = parsed_config[app_name]

    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    parsed_category_url = urllib.parse.urlparse(config['forum_category_url'])
    global forum_url
    forum_url = '{}://{}'.format(parsed_category_url.scheme, parsed_category_url.netloc)
    category_url_path = parsed_category_url.path
    category_id = get(category_url_path + '.json')['topic_list']['topics'][0]['category_id']
    parsed_wiki_url = urllib.parse.urlparse(config['wiki_url'])
    wiki_url = '{}://{}'.format(parsed_wiki_url.scheme, parsed_wiki_url.netloc)

    # Retrieve category ID.

    # Retrieve existing pages in Discourse forum.
    footer_link_re = re.compile(
        '''Il s'agit d'un sujet en provenance de l'article <a href="{}(?P<url_path>/.*?)"'''.format(re.escape(wiki_url))
        )
    for page in itertools.count(0):
        category_data = get(category_url_path + '.json?page={}'.format(page))
        for topic_digest in category_data['topic_list']['topics']:
            log.info('Retrieving topic {} "{}"'.format(topic_digest['id'], topic_digest['title']))
            topic = get('/t/{}.json'.format(topic_digest['id']))
            posts = topic['post_stream']['posts']
            main_post_digest = posts[0]
            match = footer_link_re.search(main_post_digest['cooked'])
            if match is None:
                log.warning('Ignoring existing topic {} "{}" that is not a wiki comment'.format(
                    topic['id'], topic['title']))
                continue
            wiki_page_url_path = match.group('url_path')
            wiki_page_url = urllib.parse.urljoin(wiki_url, wiki_page_url_path)
            document_slug = wiki_page_url_path.split('/', 2)[1]

            log.info('Retrieving wiki page {}'.format(wiki_page_url))
            response = urllib.request.urlopen(wiki_page_url)
            page_tree = etree.parse(response, parser=html_parser)
            title = page_tree.findtext('//div[@class="hero"]/h1[@class="title"]')
            assert title is not None
            title = title.strip()
            assert title
            title_prefix = forum_articles_prefix.get(document_slug)
            title = ' — '.join(filter(None, [title_prefix, title]))
            subtitle = page_tree.findtext('//div[@class="hero"]/h2[@class="subtitle"]')
            if subtitle is not None:
                subtitle = subtitle.strip()
            content_div = page_tree.find('//div[@class="content mkcontent is-code-dark"]')
            del content_div.attrib['class']
            assert content_div is not None
            for element in content_div.iterfind('.//div[@class="forum-ignore"]'):
                element.getparent().remove(element)
            for element in content_div.iterfind('.//div[@id="discourse-comments"]'):
                element.getparent().remove(element)
            for element in content_div.iterfind('.//script'):
                element.getparent().remove(element)
            for element in content_div.iterfind('.//a[@href]'):
                element.attrib['href'] = urllib.parse.urljoin(wiki_page_url, element.attrib['href'])
            for element in content_div.iterfind('.//img[@src]'):
                element.attrib['src'] = urllib.parse.urljoin(wiki_page_url, element.attrib['src'])
            body = (etree.tostring(content_div, encoding=str, method='html')).rstrip()
            body += """

<hr>
<small>Il s'agit d'un sujet en provenance de l'article <a href="{url}">{url}</a></small>
""".format(url=wiki_page_url)
            if subtitle:
                body = '<h2>{}</h2>\n{}'.format(subtitle, body)
            if title != topic['title']:
                log.info('Updating topic title {}: {}'.format(topic['id'], title))
                parameters = [
                    ('category_id', category_id),
                    ('title', title),
                    ('topic_id', topic['id']),
                    ]
                change = put('/t/{}'.format(topic['id']), parameters)

            posts = topic['post_stream']['posts']
            main_post_digest = posts[0]
            main_post = get('/posts/{}.json'.format(main_post_digest['id']))
            if main_post['raw'].strip() != body.strip():
                log.info('Updating topic main post {} {}'.format(topic['id'], topic['title']))
                put('/posts/{}'.format(main_post['id']), json.dumps(dict(raw=body)),
                    headers = {
                        'Content-Type': 'application/json',
                        },
                    )

        if category_data['topic_list'].get('more_topics_url') is None:
            break

    # Delete obsolete topics.
    # for topic in existing_topics:
    #     if topic['id'] not in current_topics_id:
    #         log.info('Deleting topic {} {}'.format(topic['id'], topic['title']))
    #         delete('/t/{}.json'.format(topic['id']))

    return 0


def post(path, parameters, headers = {}):
    query = urllib.parse.urlencode(dict(
        api_key=config['forum_api_key'],
        api_username=config['forum_username'],
        ))
    body = parameters.encode('utf-8') if isinstance(parameters, str) \
        else urllib.parse.urlencode(parameters).encode('utf-8')
    request = urllib.request.Request(
        data = body,
        headers = headers,
        url = urllib.parse.urljoin(forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


def put(path, parameters, headers = {}):
    query = urllib.parse.urlencode(dict(
        api_key=config['forum_api_key'],
        api_username=config['forum_username'],
        ))
    body = parameters.encode('utf-8') if isinstance(parameters, str) \
        else urllib.parse.urlencode(parameters).encode('utf-8')
    request = urllib.request.Request(
        data = body,
        headers = headers,
        method = 'PUT',
        url = urllib.parse.urljoin(forum_url, '{}?{}'.format(path, query)),
        )
    for retry in range(5):
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            if e.code == 429:  # Too Many Requests
                print("Sleeping because too many requests...")
                time.sleep(10)
            elif e.code == 500 and retry == 0:  # Internal Server Error (retry only once)
                print("Sleeping because of internal servor error...")
                time.sleep(10)
            elif e.code == 502:  # Bad Gateway
                print("Sleeping because of bad gateway...")
                time.sleep(10)
            else:
                print('Error response {}:\n{}'.format(e.code, e.read().decode('utf-8')))
                raise
        else:
            return json.loads(response.read().decode('utf-8'))
    else:
        raise Exception("Too many errors.")


if __name__ == '__main__':
    sys.exit(main())
