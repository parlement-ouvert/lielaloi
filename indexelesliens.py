#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère un fichier CSV contenant les liens externe du dossier législatif et crée un index de ces liens."""


import argparse
import csv
import os
import sys

import mako.template


template = mako.template.Template('''\
<%!
def block_trimmer(text):
    if text is None:
        return None
    while '\\n' in text:
        first_line, remaining_lines = text.split('\\n', 1)
        if first_line.strip():
            return text.rstrip()
        text = remaining_lines
    return text.rstrip()
%>


<%def name="tree(node, level)" filter="block_trimmer">
% for url_infos in (node.get('urls') or []):
${'  ' * (level - 3)}* [${url_infos['url']}](${url_infos['url']})${' (PDF)' if url_infos['pdf'] else ''}
% endfor
% for child_name, child in sorted(node.items()):
    % if child_name != 'urls':

% if level < 3:
${'£' * (level + 1)} ${child_name}
% else:
${'  ' * (level - 3)}* ${child_name}
% endif

${tree(child, level + 1)}
    % endif
% endfor
</%def>


<!-- TITLE: Liens externes -->
<!-- SUBTITLE: Index des liens externes référencés par ce dossier législatif -->

${tree(root_node, 0)}
''')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'csv',
        help='path of CSV file containing a description of each external URL',
        )
    parser.add_argument(
        'target_dir',
        help='path of directory containing Markdown text files',
        )
    args = parser.parse_args()

    root_node = {}
    with open(args.csv, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        labels = next(csv_reader)
        for row in csv_reader:
            entry = dict(zip(labels, row))
            if entry['Page 01'].strip() in ('Erreur 404', 'ko'):
                continue
            url = entry['lien'].strip()
            if not url:
                continue
            node = root_node
            for i in range(1, 12):
                label = 'Page {:02d}'.format(i)
                cell = entry[label].strip()
                if not cell:
                    break
                node = node.setdefault(cell, {})
            node.setdefault('urls', []).append(dict(url=url, pdf=entry['PDF'] == 'oui'))
    merge_singleton_nodes(root_node)

    target_dir = os.path.abspath(os.path.normpath(args.target_dir))
    assert os.path.isdir(target_dir)
    dir = os.path.join(target_dir, 'liens-externes')
    file_path = dir + '.md'
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(template.render_unicode(root_node=root_node).replace('£', '#').strip())
        file.write('\n')

    return 0


def merge_singleton_nodes(node):
    assert len(node) >= 1, node
    if len(node) > 1:
        for child_name, child in node.items():
            if child_name != 'urls':
                merge_singleton_nodes(child)
        return
    child_name = list(node.keys())[0]
    if child_name == 'urls':
        return
    child = node[child_name]
    merge_singleton_nodes(child)
    if len(child) > 1:
        return
    grand_child_name = list(child.keys())[0]
    if grand_child_name == 'urls':
        return
    grand_child = child[grand_child_name]
    del node[child_name]
    node['{} — {}'.format(child_name, grand_child_name)] = grand_child


if __name__ == '__main__':
    sys.exit(main())
