#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Ajoute à chaque article les liens vers les pages qui lui font référence.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys


link_re = re.compile(r'''(?ims)\[\s*(?P<name>.*?)\s*\]\(\s*(?P<url>.*?)(\s|\))''')


def is_between_brackets(text, i, j):
    while i >= 0:
        c = text[i]
        if c == '[':
            return True
        if c == ']':
            return False
        i -= 1
    return False


def is_individual_name(text, i, j):
    left_ok = i < 0 or text[i] in " \t\n'’"
    right_ok = j >= len(text) or text[j] in " ,."
    return left_ok and right_ok


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source_dir',
        help='path of directory containing a Markdown document with links',
        )
    parser.add_argument(
        'target_dir',
        help='path of directory containing a Markdown document without links',
        )
    args = parser.parse_args()

    source_dir = os.path.abspath(os.path.normpath(args.source_dir))
    source_dir = os.path.splitext(source_dir)[0]
    assert os.path.isdir(source_dir)
    source_file_path = source_dir + '.md'
    assert os.path.exists(source_file_path)

    target_dir = os.path.abspath(os.path.normpath(args.target_dir))
    target_dir = os.path.splitext(target_dir)[0]
    assert os.path.isdir(target_dir)
    target_file_path = target_dir + '.md'
    assert os.path.exists(target_file_path)

    for parent_dir, dir_names, filenames in os.walk(target_dir):
        for dir_name in dir_names[:]:
            if dir_name.startswith('.'):
                dir_names.remove(dir_name)
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if not filename.endswith('.md'):
                continue
            target_file_path = os.path.join(parent_dir, filename)
            if os.path.isdir(os.path.splitext(target_file_path)[0]):
                # Not a leaf
                continue
            relative_file_path = target_file_path[len(target_dir) + 1:]
            source_file_path = os.path.join(source_dir, relative_file_path)
            if not os.path.exists(source_file_path):
                continue
            with open(source_file_path, 'r', encoding='utf-8') as source_file:
                source_text = source_file.read()
            names_with_duplicate_urls = set()
            url_by_name = {}
            for match in link_re.finditer(source_text):
                name = match.group('name')
                url = match.group('url')
                existing_url = url_by_name.get(name)
                if existing_url is not None:
                    if url != existing_url:
                        print('Duplicate URLs in file {} for name "{}": {} & {}'.format(
                            source_file_path, name, existing_url, url))
                        names_with_duplicate_urls.add(name)
                else:
                    url_by_name[name] = url
            for name in names_with_duplicate_urls:
                del url_by_name[name]
            if not url_by_name:
                continue

            with open(target_file_path, 'r', encoding='utf-8') as target_file:
                current_text = target_file.read()
            new_text = current_text
            for name, url in sorted(
                    url_by_name.items(),
                    key=lambda name_and_url: (-len(name_and_url[0]), name_and_url[0]),
                    ):
                next = 0
                while True:
                    index = new_text.find(name, next)
                    if index < 0:
                        break
                    next = index + len(name)
                    if is_individual_name(new_text, index - 1, next) \
                            and not is_between_brackets(new_text, index - 1, next):
                        link = '[{}]({})'.format(name, url)
                        new_text = new_text[:index] + link + new_text[next:]
                        next += len(link) - len(name)
            if new_text == current_text:
                continue
            print('Changed {}'.format(target_file_path))
            with open(target_file_path, 'w', encoding='utf-8') as target_file:
                target_file.write(new_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
