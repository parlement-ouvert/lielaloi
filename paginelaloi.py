#! /usr/bin/env python3


# LieLaLoi -- Crée des liens entre une loi et ses documents préparatoires.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/lielaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Fait ou refait les paginatins d'un document.

Les documents doivent être au format Markdown.
"""


import argparse
import os
import re
import sys

import mako.template

from elaguelaloi import extract_table_of_contents_local_url_paths
from lavelaloi import remove_line_feeds, remove_trailing_spaces


discourse_re = re.compile(r'''(?ims)(^----*\s*\n)?\s*(?P<discourse><div id='discourse-comments'>.*?</script>)''')
paginator_div_re = re.compile(r'''(?ims)<div\s+class="forum-ignore".*?</div>''')
subtitle_re = re.compile(r'''(?ims)^\s*<!--\s*SUBTITLE:\s*(?P<subtitle>.*?)\s*-->''')
title_re = re.compile(r'''(?ims)^\s*<!--\s*TITLE:\s*(?P<title>.*?)\s*-->''')

template = mako.template.Template('''\
<%!
def inliner(text):
    if text is None:
        return None
    return ' '.join(text.split())
%>


<%def name="paginator(url_paths, index, document_title)" filter="inliner">
    % if isinstance(index, int):
<div class="forum-ignore" style="background-color: #F8F8F8; color:#9B9B9B; display: flex; font-size: 18px; justify-content: space-between; margin-bottom: 10px; margin-top: 10px; padding-bottom: 7px; padding-left: 20px; padding-right: 20px; padding-top: 7px;" width="100%">
        % if index > 0:
    <a href="${url_paths[index - 1]}" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page précédente">&lt;</a>
        % else:
    <span> </span>
        % endif
    <span style="text-align: center; text-transform: uppercase;">${document_title}</span>
        % if index + 1 < len(url_paths):
    <a href="${url_paths[index + 1]}" style="border: 1px solid; border-radius: 4px; color:#9B9B9B; padding-left: 6px; padding-right: 6px; font-weight: bolder; margin-left: 6px; margin-right: 6px; text-decoration-line: none;" title="Page suivante">&gt;</a>
        % else:
    <span> </span>
        % endif
</div>
    % endif
</%def>


${header}

${paginator(url_paths, index, document_title)}

${body}

${paginator(url_paths, index, document_title)}

${discourse}
''')


def remove_paginators(text):
    return paginator_div_re.sub(r'', text)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'title',
        help='document title',
        )
    parser.add_argument(
        'dir',
        help='path of directory containing a Markdown document',
        )
    args = parser.parse_args()

    root_dir = os.path.abspath(os.path.normpath(args.dir))
    root_dir = os.path.splitext(root_dir)[0]
    assert os.path.isdir(root_dir)
    table_of_contents_file_path = root_dir + '.md'
    assert os.path.exists(table_of_contents_file_path)
    root_url_path = '/' + os.path.basename(root_dir)
    url_paths = extract_table_of_contents_local_url_paths(root_url_path, table_of_contents_file_path)

    for index, url_path in enumerate(url_paths):
        file_path = os.path.join(root_dir, *url_path.split('/')[2:]) + '.md'
        with open(file_path, 'r', encoding='utf-8') as markdown_file:
            text = markdown_file.read()
        new_text = text
        new_text = remove_paginators(new_text)
        new_text = remove_trailing_spaces(new_text)
        new_text = remove_line_feeds(new_text)

        header, body = new_text.lstrip().split('\n\n', 1)
        title_match = title_re.search(header)
        assert title_match is not None, 'Missing title in {}'.format(file_path)
        title = title_match.group('title')
        subtitle_match = subtitle_re.search(header)
        if subtitle_match is not None:
            subtitle = subtitle_match.group('subtitle')

        discourse_match = discourse_re.search(body)
        if discourse_match is None:
            discourse = ''
        else:
            discourse = discourse_match.group('discourse')
            body = discourse_re.sub('', body)

        new_text = template.render_unicode(
            body=body,
            discourse=discourse,
            document_title=args.title.strip(),
            header=header,
            index=index,
            url_paths=url_paths,
           )

        new_text = remove_trailing_spaces(new_text)
        new_text = remove_line_feeds(new_text)
        new_text = new_text.strip() + '\n'
        if new_text != text:
            print('Changed {}'.format(file_path))
            with open(file_path, 'w', encoding='utf-8') as markdown_file:
                markdown_file.write(new_text)

    return 0


if __name__ == '__main__':
    sys.exit(main())
